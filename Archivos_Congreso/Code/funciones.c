//DEFINO VARIABLES GLOABLES A PARTIR DE LOS PARAMETROS DE ENTRADA AL CODIGO (#DEFINE ... EN GET_IMAGE-cpp)
int ancho_ruta=rows/depth_vision;
double resolution=ancho_ruta/medida_ruta;
int numero = 0;
int muestras=1/dT; //cantidad de muestras por segundo
int rango_busqueda_punto_mas_cercano=round(rows*factor_puntos_cercanos); 
int altura_puntos_ajuste_lineal=round(rows*factor_altura_puntos_ajuste_lineal);
int altura_histograma=round(rows*factor_altura_histograma);


double pixel2m(double pixels){ //transforma pixeles en metros
    return pixels*medida_ruta/ancho_ruta;
}

int m2pixel(double m){
    double aux;
    aux=m*ancho_ruta/medida_ruta;
    return (int)aux ;
}

int pix_retraso=m2pixel(retraso);
int offset_img=pix_retraso+rango_busqueda_punto_mas_cercano;

double kmh2ms(double vel){
    return vel/3.6;
}
//pasaje a m/s de la velocidad
double vel=kmh2ms(velocity);
////////////////////////////////////////////////////////////////

//llamar al final del codigo, con todas las variables guardadas en memoria en su momento
void impresor(int ndatos,double* theta_list,double* delta_list,double* tiempo_list,double* dist_list,string namefile,int* accion_control,double* vel_medida,double* vel_ref,double* error,double* concavity)
{    
	//se la llama una vez (al final de la simulacion) e imprime: tiempo, theta_error, delta steering, delta filtrado, distancia error. 
	ofstream myfile;
	
	myfile.open(namefile);
		
	if (myfile.is_open()) {
		int i;
		myfile << "Prueba sobre pista derecha-DATOS EN GENERAL -Prof. de vision " << depth_vision<<" retraso [m]"<<retraso<<" pixeles de retraso "<<pix_retraso<<" medida de ruta "<<medida_ruta<<" diametro "<<D_rueda<<" max_delta "<<max_delta<<" velocity [km/h]"<<velocity<<" K_stanley "<<K_stanley<<" dT "<<dT<<"altura de histograma "<<altura_histograma<<" ancho cajas "<<ancho_cajas<<" cant cajas apiladas "<<cantidad_cajas_apiladas<<" rows "<<rows<<" cols "<<cols<<" threshold filtro "<<threshold_filtro<<" altura ajuste puntos lineal "<<altura_puntos_ajuste_lineal<<" rango busqueda puntos cercanos "<<rango_busqueda_punto_mas_cercano<<" Tiempo corrida "<<TIEMPO_CORRIDA<<" BSIZE "<<BSIZE<<" ANCHO ruta pix "<<ancho_ruta<<" Resolution [pix/m] "<<resolution<<"canny Threshold "<<canny_Threshold<<endl;
		myfile <<"Tiempo [s]	-"<<"Theta [grados]	-"<<"Delta [grados]	-"<<"Track error	-"<<"PWM	-"<<"Vel medicion [km/h]	-"<<"Vel ref [km/h]	-"<<"error [km/h]	-"<<"concavidad		-"<<endl;
		for(i=100;i<ndatos;++i){ //pues en counter=100 comienza a moverse el auto
			myfile << tiempo_list[i] << " " << theta_list[i] << " " <<  delta_list[i]<< " " << " "<<dist_list[i]<< " "<< accion_control[i]<< " "<< vel_medida[i]<< " "<< vel_ref[i]<<" "<<error[i]<<" "<<concavity[i]<<endl;
		}
		myfile.close();
	}else{
		cout<< "no pudo abrir archivo de datos";
	}

}

void impresor_imagenes(Mat* imagenes,int largo){
    //int filas=(sizeof(imagenes)/sizeof(imagenes[0]));
    //int columnas=(sizeof(imagenes[0])/sizeof(imagenes[0][0]));
      
    String folder="";
    for (int i=0;i<largo;i++)
    {
    
	String txt1 = "./"+folder+"/cruda_" + to_string(3*i) + ".png";
	String txt2 = "./"+folder+"/warpeada_" + to_string(3*i+1) + ".png";
	String txt3 = "./"+folder+"/control_" + to_string(3*i+2) + ".png";
	imwrite(txt1,imagenes[3*i]);
	imwrite(txt2,imagenes[i*3+1]);
	imwrite(txt3,imagenes[i*3+2]);
    }
    //imwrite("test.tiff", imagenes);
    printf("Multiple files saved\n");

    return;
}

//----------------------------------------------------
//
// METHOD:  polyfit
//
// INPUTS:  dependentValues[0..(countOfElements-1)]
//          independentValues[0...(countOfElements-1)]
//          countOfElements
//          order - Order of the polynomial fitting
//
// OUTPUTS: coefficients[0..order] - indexed by term
//               (the (coef*x^3) is coefficients[3])
//
//----------------------------------------------------
int polyfit(const double* const dependentValues,
            const double* const independentValues,
            unsigned int        countOfElements,
            unsigned int        order,
            double*             coefficients)
{
    // Declarations...
    // ----------------------------------
    enum {maxOrder = 5};
    
    double B[maxOrder+1] = {0.0f};
    double P[((maxOrder+1) * 2)+1] = {0.0f};
    double A[(maxOrder + 1)*2*(maxOrder + 1)] = {0.0f};

    double x, y, powx;

    unsigned int ii, jj, kk;

    // Verify initial conditions....
    // ----------------------------------

    // This method requires that the countOfElements > 
    // (order+1) 
    if (countOfElements <= order)
        return -1;

    // This method has imposed an arbitrary bound of
    // order <= maxOrder.  Increase maxOrder if necessary.
    if (order > maxOrder)
        return -1;

    // Begin Code...
    // ----------------------------------

    // Identify the column vector
    for (ii = 0; ii < countOfElements; ii++)
    {
        x    = dependentValues[ii];
        y    = independentValues[ii];
        powx = 1;

        for (jj = 0; jj < (order + 1); jj++)
        {
            B[jj] = B[jj] + (y * powx);
            powx  = powx * x;
        }
    }

    // Initialize the PowX array
    P[0] = countOfElements;

    // Compute the sum of the Powers of X
    for (ii = 0; ii < countOfElements; ii++)
    {
        x    = dependentValues[ii];
        powx = dependentValues[ii];

        for (jj = 1; jj < ((2 * (order + 1)) + 1); jj++)
        {
            P[jj] = P[jj] + powx;
            powx  = powx * x;
        }
    }

    // Initialize the reduction matrix
    //
    for (ii = 0; ii < (order + 1); ii++)
    {
        for (jj = 0; jj < (order + 1); jj++)
        {
            A[(ii * (2 * (order + 1))) + jj] = P[ii+jj];
        }

        A[(ii*(2 * (order + 1))) + (ii + (order + 1))] = 1;
    }

    // Move the Identity matrix portion of the redux matrix
    // to the left side (find the inverse of the left side
    // of the redux matrix
    for (ii = 0; ii < (order + 1); ii++)
    {
        x = A[(ii * (2 * (order + 1))) + ii];
        if (x != 0)
        {
            for (kk = 0; kk < (2 * (order + 1)); kk++)
            {
                A[(ii * (2 * (order + 1))) + kk] = 
                    A[(ii * (2 * (order + 1))) + kk] / x;
            }

            for (jj = 0; jj < (order + 1); jj++)
            {
                if ((jj - ii) != 0)
                {
                    y = A[(jj * (2 * (order + 1))) + ii];
                    for (kk = 0; kk < (2 * (order + 1)); kk++)
                    {
                        A[(jj * (2 * (order + 1))) + kk] = 
                            A[(jj * (2 * (order + 1))) + kk] -
                            y * A[(ii * (2 * (order + 1))) + kk];
                    }
                }
            }
        }
        else
        {
            // Cannot work with singular matrices
            return -1;
        }
    }

    // Calculate and Identify the coefficients
    for (ii = 0; ii < (order + 1); ii++)
    {
        for (jj = 0; jj < (order + 1); jj++)
        {
            x = 0;
            for (kk = 0; kk < (order + 1); kk++)
            {
                x = x + (A[(ii * (2 * (order + 1))) + (kk + (order + 1))] *
                    B[kk]);
            }
            coefficients[ii] = x;
        }
    }

    return 0;
}

Vec3b BGR(Mat imagen, int row, int col){
    Vec3b bgrPixel = imagen.at<Vec3b>(col,row);
    //printf("[%d %d %d]\n",bgrPixel[0],bgrPixel[1],bgrPixel[2]);
    return bgrPixel;
}


double pol(unsigned int order, double* coef,double x1){ //evalua el polinomio (en variables x1,y1) de coeficientes coef y orden order, en el valor x1. Devuelve el valores de f(x1)=y1
    //formato: f=coef[0] + coef[1] * x**1 + ... + coef[n] * x**n
    double f=0;
    for (int i=0;i<order+1;i++){
         f=f+coef[i]*pow(x1,i);
    }
    return f;
}

void dibuja_zona(double* coef_l, double* coef_r, int order_l, int order_r,Mat & imagen, int ordenada){//recibe una imagen, dos polinomios (en varibales x1,y1)y un valor de ordenada en x1. Marca la zona interna a las curvas en verde.
    Point p;
    for (int y=ordenada;y<rows;y++){
        for (int x=0;x<cols;x++){
            p=Point(x,y);
            if (p.x>pol(order_l,coef_l,p.y) && p.x<485){
                circle( imagen,
                p,
                0.0001,
                Scalar( 0, 0, 0 ),
                FILLED,
                LINE_8 );
            }
        }
     }
     return;
}

void filter(Mat& original,Mat& imagen_filtered){ //aplica un filtro blanco-negro a la imagen: si el color esta por debajo de un threshold lo pone en negro y sino en blanco
    Mat gray_image;
    cvtColor(original,gray_image,7,0);

    //imshow("imagen gris con bloqueo",gray_image);*/
    ///////////////
    int th_val=threshold_filtro;
    threshold(gray_image,imagen_filtered, th_val, 255, THRESH_BINARY);  //BGR_planes[2]
    //imshow("Filter image",imagen_filtered);
    
    //original = gray_image.clone();
    
    return;
}


//funcion que aplica un filtro tipo Canny
void canny_edge_filter(Mat src,Mat& dst){
    Mat src_gray;
    Mat detected_edges;
    
    const int ratio = 3;
    const int kernel_size = 3;

    cvtColor( src, src_gray, COLOR_BGR2GRAY );
    
    blur( src_gray, detected_edges, Size(3,3) );
    Canny( detected_edges, detected_edges, canny_Threshold, canny_Threshold*3, kernel_size );
    dst = Scalar::all(0);
    src.copyTo( dst, detected_edges);
    
    cvtColor(dst,dst,7,0);
    
    return;
}


float deg2rad(float var) {
	return M_PI*var/180.0;
}

void puntos_warpeo(Point2f* src, Point2f* dst){ //marca los puntos para hacer el warpeo. Se calibra una vez con una imagen de ruta recta. 
    //Coordenadas de un cuadrado visto en perspectiva, cuya base coincide con el comienzo de vision de lÃ­neas, y cuyos lados coinciden con el ancho de la ruta.
    float center=cols/2;
	
    float ancho_base=484.0/3;
    float ancho_cima=258.0/3;
    float altura_cima=375.0/3;
    src[0]=Point2f(center-(ancho_base)/2,rows-1);
    src[1]=Point2f(center+(ancho_base)/2,rows-1);
    src[2]=Point2f(center+(ancho_cima+1)/2,altura_cima); 
    src[3]=Point2f(center-(ancho_cima+1)/2,altura_cima);      //estos valores se ingresan manualmente, NO TOCAR
    
    //El cuadrado anterior debe transformarse en un cuadrado "en planta". 
    
    int lado=rows/depth_vision; //permite ver "depth_vision" cuadrados
    //Luego con la variuable retraso podemos indicarle al codigo dÃ³nde efectivamente estÃ¡n las ruedas delanteras respecto el comienzo de visiÃ³n de las lÃ­neas.
    
    int delta_dst=lado/2; //cuÃ¡n separados respecto al centro estÃ¡n los puntos de la imÃ¡gen dest 

    dst[0]=Point2f(center-delta_dst,rows-1); 
    dst[1]=Point2f(center+delta_dst,rows-1);
    dst[2]=Point2f(center+delta_dst,rows-1-lado);
    dst[3]=Point2f(center-delta_dst,rows-1-lado);
}

void vision_camera(Point2f* src,Mat& imagen){
	int r1x,r1y,r2x,r2y,ind,i;
	Point p1,p2;
	for (i=0;i<4;i++){
	    r1x=(int)src[i].x;
	    r1y=(int)src[i].y;
		p1.x=r1x;
		p1.y=r1y;
		
		ind=i+1;
		if (i==3){
			ind=0;
		}
	    r2x=(int)src[ind].x;
	    r2y=(int)src[ind].y;
		p2.x=r2x;
		p2.y=r2y;
		
		line(imagen, p1,p2, CV_RGB(255.0,0.0,0.0),2);
	}
}


void draw_box(Point2f p1,Point2f p2, Mat& imagen){ //recibe el punto inf/izq y el sup/der y crea un rectangulo en imagen
    if (p1.x<0){
        p1.x=0;
    }
    
    if (p2.x>=cols){
        p2.x=cols-1;
    }
    
    if (p1.y>=rows){
        p1.y=rows-1;
    }
    
    if (p2.y<0){
        p2.y=0;
    }
    
    
    rectangle( imagen,
         p1,
         p2,
         Scalar( 255, 255, 255 ),
         LINE_4 );
	return;
}

void draw_box_color(Point2f p1,Point2f p2, Mat& imagen){ //recibe el punto inf/izq y el sup/der y crea un rectangulo en imagen
    if (p1.x<0){
        p1.x=0;
    }
    
    if (p2.x>=cols){
        p2.x=cols-1;
    }
    
    if (p1.y>=rows){
        p1.y=rows-1;
    }
    
    if (p2.y<0){
        p2.y=0;
    }
    
    
    rectangle( imagen,
         p1,
         p2,
         Scalar( 0, 255, 0 ),
         LINE_4 );
	return;
}

void find_white(Point p1,Point p2, Mat& imagen, vector<Point> &lista){//recibe los puntos que marcan un rectangulo dentro de imagen  y guarda el lista aquellos que tiene  un color mayor a un threslhold (casi blancos)
    int x_min=p1.x; 
    int x_max=p2.x;
    
    int y_min=p2.y;
    int y_max=p1.y;
    int color;
	
    if(x_min<0){
        x_min=0;
    }
    if(x_max>=cols){
        x_max=cols-1;
    }
    if(y_min<0){
        y_min=0;
    }
    if(y_max>=rows){
        y_max=rows-1;
    }
    
    //printf("P1(%d ,%d) -  P2(%d ,%d)\n", x_min,y_max, x_max,y_min);
    
    for(int i=x_min;i<x_max;i++){
        for (int j=y_min;j<y_max;j++){
	        color=imagen.at<uchar>(j,i);
		    //printf("%d = %d ,%d\n",i,x_min,x_max);
            if (color>threshold_find_white && j>altura_puntos_ajuste_lineal){                        //como el ajuste serÃ¡ lineal y SOLO sobre los puntos inferiores de la imagen,
//defino este valor lÃ­mite para los puntos que serÃ¡n considerandos pertenecientes a la ruta. 
            
                lista.push_back(Point2f(i,j));
                //printf("encontrÃ³ blancos!\n");
		//printf("entrÃ³\n");
		//printf("%d ,%d\n",i,j);
            }
            
        }
    }
}



void dibuja_cruz(Mat & imagen,Point punto){
	int ancho =5;
	int x0=punto.x;
	
	if (x0<0 || x0>cols){
	    return;
	}
	
	line(imagen,Point(punto.x-ancho,punto.y-ancho),Point(punto.x+ancho,punto.y+ancho) , 255,2);
	line(imagen,Point(punto.x+ancho,punto.y-ancho),Point(punto.x-ancho,punto.y+ancho) , 255,2);
	return;
}

int X2x(int X){ //funciones para pasar de un sistema de coords a otro
    return X+cols/2;
}


int Y2y(double Y){
    return (int)(rows-Y);
}

int x2X(int x){
    return x-cols/2;
}

double y2Y(int y){
    return rows -y;
}


void dibuja_fitteo(double* coef,Mat& imagen,int order){ //recibe un polinomio en variables x1,y1 y lo dibuja en la imagen
    //Polinomio del ajuste: coef[n]*x**n
    //primero se pasa a variables de imagen x,y
    int x[rows+offset_img],y[rows+offset_img];
    
    for (int i=0;i<rows+offset_img;i++){
        y[i]=i;
        x[i]=(int)pol(order,coef,i );
        //printf("x: %d - y: %d\n",x[i],y[i]);
    }
    
    Point p1,p2;    
    for(int i=offset_img;i<rows+offset_img;i++){      
        if (x[i]>-1 && x[i]<cols-1 && x[i+1]>-1 && x[i+1]<cols-1){
            p1.x=x[i];
            p1.y=y[i]-offset_img;
            p2.x=x[i+1];
            p2.y=y[i+1]-offset_img;    
            line(imagen,p1,p2,Scalar(0,0,255),5,LINE_8);
        }
    }
}



int get_base(double* coef, int order){//recibe un polinomio en variables x1,y1 y encuentra donde empieza la ruta (Y=0 o y=255 o x1=255). Devuelve en variable X el pixel donde ocurre el cruce por cero. Es una aproximacion del punto donde evalua Stanley
    int x1=rows-1;
    int y1_cruce=(int)pol(order,coef,x1);
    
    return x2X(y1_cruce);
}

void print_vector_points(vector<Point> &lista){ // recibe un vector<point> y lo imprime por pantalla
    /*for(int i=0;i<lista.size()){
        printf("%d:  [x=%d , y=%d]\n",i,lista.at(i).x,lista.at(i).y);
        return; 
    }*/
	int i=0;
	for(const auto & value: lista) {
	    printf("%d:  [x=%d , y=%d]\n",i++,value.x,value.y);
	}
	        
}
	

void print(vector<int> vect)// recibe un vector<int> y lo imprime por pantalla
{
    for (int i=0; i<vect.size(); i++) 
       cout << vect[i] << " \n"; 
    return;
}


void histogram(int a,int high, vector<int> &rx,vector<int> &frec,Mat imagen){ //crea un histograma de franjas verticales de ancho a midiendo la cant de puntos blancos hasta una altura high y guarda los resultados en rx y frec. 
    int div=cols/a;
    int suma=0;
    int color;
    //printf("altura a recorrer: %d \n",high);
    for(int i=0;i<div;i++){
        rx.push_back(a*i+a/2);
        
        for (int j=0;j<a;j++){
            for (int w=0;w<high;w++){
                color=imagen.at<uchar>(rows-w-1,i*a+j);    //fila - columna
                if(color > 50 ){   //este valor podria aumentarse pero asi viene funcionando.
                    suma=suma+1;
                    //printf("i: %d    -  j:%d   -    w:%d    -    \n",i,j,w);
                }
            }
                        
        }
        frec.push_back(suma);
        suma=0;
    }

}

void find_max(vector<int> rx,vector<int> frec,int* start_points){ //recibe el histograma y encuentra el maximo y segundo maximo. Guarda en start_points la ubicacion en pixeles (en x) de la ruta izquierda y derecha respectivamente. La mejora que se hizo a esta funcion consiste en: el histograma se usa para ncontrar la posicion de las lineas solo para la primer imagen. Luego, al pasar el tiempo el origen de las rutas se actualiza buscandi un promedio en la caja de la imagen anterior (uso de la funcion new_center). Es decir, find_max sirve solo cuando el vehiculo esta arrancando y sabemos que esta centrado (es decir, las dos lineas estan en pantalla y estan a la izq y derecha del centro).

    //recordar que los pixeles se miden en x de izq a derecha
    int max=0, r=0,posicion=0;
    
    if(frec.size()!= rx.size()){
        printf("revisar longitudes del histograma\n");
    }
    
    for (int i=0;i<frec.size();i++){ 
        if(frec[i]>max){
            max=frec[i]; //cantidad de cuentas
            r=rx[i]; //posicion del maximo en numero de pixel
            posicion=i; //posicion del maximo en casilla del histograma
          
        }    
    }
    //printf("maximo encontrado en la posicion %d, valor %d y casilla de valor %d\n",posicion,max,r);
    
    int delta=(rx[1]-rx[0]); //ancho de cada franja del histograma en pixeles
    int posicion_r=posicion+ancho_ruta/delta; //posicion de la posible linea a la derecha/izquierda en casilla del histograma
    int posicion_l=posicion-ancho_ruta/delta;
    //printf("posisicon derecha: %d   - posisicon izquierda: %d\n",posicion_r,posicion_l);
    
    if (r<cols/2 && r>-1){
        start_points[0]=r; //posicion den pixel de la primer linea de ruta
        start_points[1]=rx[posicion_r];
        //printf("LINEA IZQUIERDA en pixel: %d  - LINEA DERECHA en pixel: %d\n",start_points[0],start_points[1]); 
        return;
    }
    if (r>cols/2 && r<cols){
        start_points[1]=r;
        start_points[0]=rx[posicion_l];
        //printf("LINEA IZQUIERDA en pixel: %d  - LINEA DERECHA en pixel: %d\n",start_points[0],start_points[1]); 
        return;
    }
    
    //printf("PROBLEMAS PARA HALLAR EL MÃXIMO DEL HISTOGRAMA\n");
    start_points[0]=-1;
    start_points[1]=-1;
    return; 
}


int new_center(Mat imagen,Point bottom_left, Point top_right){ //calcula el promedio de puntos blancos en una caja y devuelve ese valor (trabaja en x). 
//si no encuentra nada en esa caja, devuelve -1.
    int x_min=bottom_left.x;
    int x_max=top_right.x;
    
    int y_min=top_right.y;
    int y_max=bottom_left.y;
    
    
    if(x_min<0){
        x_min=0;
    }
    
    if(x_max>=cols){
        x_max=cols-1;
    }
    //printf("y_min: %d  - y_max: %d\n",y_min,y_max);
    if (y_min<0){
        y_min=0;
    }
    
    if (y_max>=rows){
        y_max=rows-1;
    }
    
    Scalar color;
    int counter=0;
    int suma=0;
    
    for (int i=x_min;i<x_max;i++){
        for (int j=y_min;j<y_max;j++){
            color=imagen.at<uchar>(j,i); //fila - columna
            if (color.val[0]>threshold_find_white){ //este valor resultÃ³ andar bien y por eso lo dejÃ© asÃ­.
                //printf("encontrÃ³ blancos en la divisiÃ³n %d. En el pixel x:%d - y:%d\n",(256-y_max)/32+1, i,j);
                counter=counter+1;
                suma=suma+i;
            }
        }
    }
    if (counter<5){
        //printf("No encontrÃ³ nada para la ultima iteraciÃ³n!\n");
        return -1;
    }
    int x0_new=suma/counter; //promedio de todos los blancos de ese cuadrado
    //printf("nuevo centro de la division %d: %d\n",(256-y_max)/32,x0_new);
    return x0_new;
}


void inicializa(double* x1, double* y1, vector<Point> lista){ //recibe un vector<point> en x,y y crea dos arreglos en x1,y1 cÃ³modos para hacer fiteo
    int i=0;
	for(const auto & value: lista) {
	    x1[i]=value.y;
	    y1[i]=value.x;
	    i++;
	}
	
	for(int j=0;j<lista.size();j++){
        //printf("PIXEL %d-Ã©simo: [%f , %f]\n",j,X[j],Y[j]);
	}//altura a recorrer: 64 *//*
    return;
}

    
void promedio(vector<Point> lista, double* media){
    double mediax=0, mediay=0;
    int i=0;
	for(const auto & value: lista) {
	    mediax=mediax+(value.x);
	    mediay=mediay+(value.y);
	    //printf("mediax: %f - mediay: %f\n",mediax,mediay);
	    i++;
	}    
	
	mediax=mediax/(i+1);
    mediay=mediay/(i+1);
    //printf("mediax: %f - mediay: %f\n",mediax,mediay);
    media[0]=mediax;
    media[1]=mediay;
    
    //print_vector_points(lista);
    printf("Promedio de los puntos en x,y: [%f - %f]\n",media[0],media[1]);
    return;
}    

void RMSE(vector<Point> lista, double* RMSE,double* media){
    promedio(lista,media);
    double rmsex=0, rmsey=0;
    
    int i=0;
	for(const auto & value: lista) {
	    rmsex=rmsex+pow(value.x - media[0],2);
	    rmsey=rmsey+pow(value.y - media[1],2);
	    i++;
	}     
	//printf("rmsex: %f - rmsey: %f\n",rmsex,rmsey);
	rmsex=sqrt(rmsex/(i+1));
    rmsey=sqrt(rmsey/(i+1));
    //printf("rmsex: %f - rmsey: %f\n",rmsex,rmsey);
    RMSE[0]=rmsex;
    RMSE[1]=rmsey;
    printf("Raiz media cuadrÃ¡tica en x,y: [%f  -  %f]\n",RMSE[0],RMSE[1]);
    return;
}


int box_center(Point bottom_left,Point top_right){
    return (int)((top_right.x+bottom_left.x)/2);
}


void print_polinomio(int order,double*coef){
    printf("Polinomio del Ajuste (x1,y1) de orden %d:  y1= ",order);
    for(int i=0;i<order+1;i++){
        printf("%f x1**(%d) + ",coef[i],i);
    }
    printf("\n");       
}  

void FITEO(vector<Point> puntos,unsigned int order, double* coefficients){ //recibe un vector de puntos "puntos" (en x,y), un orden de polinomio y guarda los ecoeficientes del ajuste en coefficientes (el polinomio trabajarÃ¡ en variables x1,y1
    unsigned int countofelements=puntos.size();
    double x1[countofelements], y1[countofelements]; //cambio a estas variables cÃ³modas para hacer fiteo
    inicializa(&x1[0],&y1[0],puntos);
    
    int v=polyfit(x1,y1,countofelements,order,coefficients);
    //print_polinomio(order,coefficients);
    
}
    
void derivada(double* f, int order, double* f_1){ //recibe polinomio y calcula los coeficientes de su derivada (todo en variables x1,y1)
    for (int i=0;i<order;i++){
        f_1[i]=f[i+1]*(i+1);
    }
    //printf("Dervidad primera: ");
    //print_polinomio(order-1,f_1);
    return;
} 

double angulo(double* coef, int order, int* base){//recibe el polinomio en (x1,y1) de orden "Order" y el punto base (en x1) donde debe evaluar la derivada para conocer finalmente el angulo de error respecto a la direccion del auto.

    double f_1[order-1];
    derivada(coef,order,f_1);
        
    //evaluo en el punto base
    double f_1_base =pol(order-1,f_1,base[0]); //es la tangente dy1/dx1 en el punto mas cercano a  la curva
    //printf("derivada en x1,y1: %f\n",f_1_base);
    if (abs(f_1_base)<0.005){
        return 0; 
    }
    else{
        if (f_1_base>0){
            return -90 - atan(-1/f_1_base)*180/PI;
            //return -90 - atan2(-1,f_1_base)*180/PI;
        }
        else{   
            return 90 - atan(-1/f_1_base)*180/PI;
            //return   90 - atan2(-1,f_1_base)*180/PI;
        }
    }
}              


void MyFilledCircle( Mat& img, Point center )
{
  circle( img,
      center,
      5,
      Scalar( 255, 0, 0 ),
      FILLED,
      LINE_8 );
}

double modulo(double x, double y){
    return sqrt(pow(x,2)+pow(y,2));
}

void min_dist(double* coef, int order,int* base){ //recibe un polinomio (en x1,y1) y guarda en base el punto (en x1,y1) mÃ¡s cercano de la curva del polinomio al punto delantero del auto (perpendicular desde la curva al auto).
    
    int x1_0=rows-1+pix_retraso; //en realidad la cÃ¡mara nos entrega una vision de la ruta en una posicion mÃ¡s adelante respecto al verdadero frete del auto. Debo entonces corregir la posicion del frente del auto, que estÃ¡ fuera de la imagen warpeada. Para esto se hace una calibraciÃ³n segpun la posiciÃ³n y orientacion de la camara.
    //la calibracion constÃ³ de poner bloques de distancia 1m y observar dÃ³nde mira la cÃ¡mara
    int y1_0=cols/2;
    
    double dist=1000, value;
    int x1_base,y1_base;
    
    
    for (int i=rows-rango_busqueda_punto_mas_cercano;i<rows+rango_busqueda_punto_mas_cercano;i++){//215-295
        value=modulo(x1_0-i,y1_0-pol(order,coef,i));
        if (value<dist){
            dist=value;
            x1_base=i;
            y1_base=(int)pol(order,coef,i);
            //printf("valores cercanos al auto en posicion %d,%d corresponde al pixel x1,y1: %d - %f\n",y1_0,x1_0,i,pol(order,coef,i));
        }
    }
    //sospecho que hay problemas con esta funcion luego de pruebas con curvas, simplifquemos lo que hace y veamos
    //x1_base=rows-1;
    //y1_base=(int)pol(order,coef,x1_base);
    
    base[0]=x1_base;
    base[1]=y1_base;
    //printf("punto base (en pixeles x,y): %d,%d\n",base[1],base[0]);
    return;
}

double ms2rs(double speed){
    return speed/(D_rueda/2);//recibe velocidad en m/s y devuelve en rad/s
}

double track_error(int* base,int left){
    //recibe el punto de la ruta mas cercano al auto en variables x1,y1 almacenadas en base. base[0] contiene x1 o y / base[1] contiene y1 o x
    //finalmente calcula el error de seguimiento, sabiendo de que linea esta mirando a traves de left
    
    double e=0.0;
    
    if (left==1){ //indica que estoy trabajando con la linea izq
	
	if (base[1]>=cols/2){
	    e=-1*pixel2m( modulo(rows-1+pix_retraso-base[0],cols/2-base[1]) + ancho_ruta/2);
	}
	else{
	    e=pixel2m( modulo(rows-1+pix_retraso-base[0],cols/2-base[1]) - ancho_ruta/2);
	}
    }
    else{//indica que estoy trabajando con la linea der
	
	if (base[1]>=cols/2){
	    e=pixel2m( - modulo(rows-1+pix_retraso-base[0],cols/2-base[1]) + ancho_ruta/2);
	}
	else{
	    e=pixel2m( modulo(rows-1+pix_retraso-base[0],cols/2-base[1]) + ancho_ruta/2);
	}
    }
    return e;
}

void ruta(vector<Point>& list,int x0, int ancho, int alto, Mat imagen_warped, Mat& boxes){
//recibe una imagen warpeada y filtrada. Recibe x0 (dÃ³nde poner la primer caja para comenzar la busqueda de puntos blancos de cada curva de la ruta). La bÃºsqueda la hace poninedo rectÃ¡ngulos (donde buscar puntos blancos) de ancho "ancho" y un altura dada tal que entran "divs" rectangulos en toda la imagen. Los puntos que encuentre los guarda en list.
	int divs=cantidad_cajas_apiladas; //cantidad de cajas que entran apiladas verticalemten en la imagen 
	Point bottom_left=Point(x0-ancho/2, rows-1);
	Point top_right=Point(x0+ancho/2, rows-1-alto);	//puntos (x-y) que definen el rectangulo de la iteraciÃ³n i-Ã©sima
	    
        vector<Point> list_temporary[divs]; //en este vector se guardan los puntos de la linea que can dentro del rectangulo de la iteracion iÃ©sima. Me sirven para ubicar la caja imnediatamente superior. Se borrar en cada iteracion, pero quedan guardados en list.
        int order_1=1;
        double coef_temporary[order_1+1];
        
        int tol=5;
        for (int i=0;i<divs;i++){
	        int error=tol+10;
	        while (error>tol && x0>0){ //voy a corregir el valor de x0 hasta que estÃ© seguro de que la caja estÃ¡ bien centrada
	            x0=new_center(imagen_warped,bottom_left,top_right);  //actualizo el centro de cada caja buscando un promedio de los puntos que hay dentro de la caja. Si no hay puntos, x0=-1 y termina la bÃºsqueda
	            error=abs( x0-box_center(bottom_left,top_right) ); //me fijo cuÃ¡nto movÃ­ la caja en esta iteraciÃ³n
	            bottom_left=Point(x0-ancho/2, rows-1-i*alto);
		    top_right=Point(x0+ancho/2, rows-1-i*alto-alto);	//actualizo los bordes de la caja segÃºn el x0 corregido
	        }
		    
		    if (x0<1){//significa que no encontrÃ³ ningÃºn punto en la caja (por ejemplo, se saliÃ³ de pantalla la ruta
		        break;
		    }
		    
		    dibuja_cruz(boxes,Point(x0,rows-1-i*alto-alto/2)); //dibujo una cruz en el nuevo centro donde ubico la caja para ayuda visual

            find_white(bottom_left,top_right,imagen_warped,list);	//busco todos los puntos blancos que cayeron adentro de esa caja 
            find_white(bottom_left,top_right,imagen_warped,list_temporary[i]);
            
		    draw_box(bottom_left,top_right,boxes); //dibujo los cuadrados donde se hizo la bÃºsqueda para ayuda visual
		    
		    
	        //para resolver dÃ³nde centrar la caja de arriba hago un ajuste de orden 1 de los puntos de la caja actual
	        FITEO(list_temporary[i],order_1,coef_temporary);
	        //dibuja_fitteo(coef_temporary,boxes,order);
	        x0=(int)pol(order_1,coef_temporary,rows-1-(i+1)*alto);
	        bottom_left=Point(x0-ancho/2, rows-1-(i+1)*alto);//actauliza los cuadrados estimados para la iteraciÃ³n i+1
		top_right=Point(x0+ancho/2, rows-1-(i+2)*alto);
    	}
    	
    	return;
}

double Rc(double* coef,int order,int* base){
    double f_1[order];
    double f_2[order-1]; //requiere un polinomio de orden al menos 2 
    
    derivada(coef,  order, f_1);
    derivada(f_1,order-1,f_2);
    
    double r=pow(1+pow(pol(order-1, f_1,190),2),3.0/2)/abs(pol(order-2,f_2,190));
    //r=r/abs(pol(order-2,f_2,base[0]));
    return pixel2m(r);
}

double ms2kmh(double vel){
    return vel*3.6;
}

void desplaza_imagen(Mat& imagen,int delta){
    for (int y=0;y<rows-delta;y++){
	for (int x=0;x<cols;x++){
	    //get pixel
	    Vec3b & pixel1=imagen.at<Vec3b>(Point(x,y));
	    Vec3b & pixel2=imagen.at<Vec3b>(Point(x,y+delta));	   
	    // set pixel
	    pixel1 = pixel2;
	}
    }

    for (int y=rows-delta;y<rows;y++){
	for (int x=0;x<cols;x++){
	    //get pixel
	    Vec3b & pixel1=imagen.at<Vec3b>(Point(x,y));
	    pixel1.val[0]=0;
	    pixel1.val[1]=0;
	    pixel1.val[2]=0;
    
	}
    }
}

void plot_min_dist(int* base,Mat& imagen){
	// printf("PUNTO BASE: x,y=[%d - %d]\n",base[1],base[0]);
	MyFilledCircle(imagen,Point(base[1],base[0]-offset_img));

	//dibujo la representaciÃ³n del auto
	Point p1,p2;
	
	p1.x=cols/2;
	p1.y=rows+pix_retraso-offset_img;
        p2.x=cols/2;
        p2.y=rows+pix_retraso-offset_img+20;    
	line(imagen,p1,p2,Scalar(100,100,100),5,LINE_8);
	return;
}


int image_process(int* start_points,double* theta_e_list,double* dist_list, Mat imagen,Mat* imagenes_RAM, double* concavity ) {
    unsigned t0, t1;
    
    //valores de la iteracion actual que necesito para determianr el angulo de las ruedas delanteras
    double dist,theta_e;   

    //FILTRADO 
    Mat imagen_filtered;
    imagen_filtered.create( imagen.size(), imagen.type() );
    filter(imagen,imagen_filtered); //(tarda 15ms)
    
    //canny_edge_filter(imagen,imagen_filtered);//cuanto tarda?

    //creacion de puntos para el warpeado (basada en una imagen de pista derecha de simulacion)
    Point2f src[4], dst[4];
    puntos_warpeo(src,dst);
	
    //ayuda visual - lineas de warpeado en imagen original
    //vision_camera(src,imagen); //(no tarda nada)
	
    
    //warpeado
    Mat imagen_orig=imagen_filtered.clone();
    Mat imagen_warped=imagen_filtered.clone();
    
	Mat trans_mat33 = getPerspectiveTransform(src, dst);
	
	warpPerspective(imagen_orig, imagen_warped, trans_mat33,imagen.size());	 //(tarda 100ms, el resto del codigo tarda 90)
    
	Mat boxes=imagen_warped.clone();//acÃ¡ se dibujarÃ¡n los rectangulos de la busqueda y el fiteo 
       
    //OBTENGO LOS VALORES X-Y DE PUNTOS DE LA CURVA
    vector<int> rx; //almacena el centro de cada franja vertical del histograma
    vector<int> frec;    //almacena la cantidad de blancos dentro de cada franja vertical del histograma

	
    Point bottom_left_0L, top_right_0L;
    Point bottom_left_0R, top_right_0R;
    //defino las dimensiones de los cuadrados donde buscar
    int alto=rows/cantidad_cajas_apiladas; //cantidad_cajas_apiladas; 
    int ancho=ancho_cajas; //ancho de cada caja
    int divs=cantidad_cajas_apiladas;//cantidad de cajas que entran en y
	
	if (numero==0 || concavity[numero-1]==-1.0){		
	    //histograma devuelve puntos de inicio de la ruta
	    int a=ancho_cajas; //cada casilla tendrÃ¡ a (puntos horizontalmente)
	    int high=altura_histograma; //solo voy a mirar una parte inferior de la imagen total. Bajo valor respecto a SIMULACION (10)
	    
	    histogram(a,high,rx,frec,imagen_warped);//construye el histograma
	    /*printf("rx:\n");
	    print(rx);
	    printf("frec:\n");
	    print(frec);*/
        
	    find_max(rx,frec,&(start_points[0])); //toma el histograma y se fija dÃ³nde ocurren los mÃ¡ximos: define el inicio de cada linea de ruta y lo almacena en start_points (variable x)
	    //cout<<start_points[0]<< "  "<<start_points[1]<<"\n";
        
    }
    else{
    //actualiza la posicion en X donde inicia la ruta en funcion de dÃ³nde estaba en la corrida anterior 
    
        if (start_points[0]>-1){
            bottom_left_0L=Point(start_points[0]-ancho/2,rows-1);
            top_right_0L=Point(start_points[0]+ancho/2,rows-1-rows/divs);
            start_points[0]=new_center(imagen_warped,bottom_left_0L,top_right_0L);
            }
	    
        else if (start_points[1]>-1){//si se perdiÃ³ la ruta izquierda debo buscarla no desde -1 (pixel) sino desde cierto valor positivo. Experimentalmente se probÃ³ y resulta que la ruta vuelve a aparecer en un pixel positivo y no dese el pixel cero
            for (int i=start_points[1]-ancho_ruta;i>0;i--){
		//if (i<cols/2){
		    bottom_left_0L=Point(i-ancho/2,rows-1);
		    top_right_0L=Point(i+ancho/2,rows-1-alto);
		    start_points[0]=new_center(imagen_warped,bottom_left_0L,top_right_0L);
		    if(start_points[0]>-1){
			break;
		    }
	    }
            //}
        }
        //lo dibuja
        bottom_left_0L=Point(start_points[0]-ancho/2,rows-1);
        top_right_0L=Point(start_points[0]+ancho/2,rows-1-alto);
        draw_box(bottom_left_0L,top_right_0L, boxes);
	
	//actualizo el rectangulo de la derecha
        if(start_points[1]>-1){
	    bottom_left_0R=Point(start_points[1]-ancho/2,rows-1);
	    top_right_0R=Point(start_points[1]+ancho/2,rows-1-rows/divs);
	    start_points[1]=new_center(imagen_warped,bottom_left_0R,top_right_0R);
	}
	else if (start_points[0]>-1){
            for (int i=start_points[0]+ancho_ruta;i<cols;i++){
		//if (i>cols/2){
		    bottom_left_0R=Point(i-ancho/2,rows-1);
		    top_right_0R=Point(i+ancho/2,rows-1-alto);
		    start_points[1]=new_center(imagen_warped,bottom_left_0R,top_right_0R);
		    if(start_points[1]>-1){
			break;
		    }
	    //}
            }
        } 
	//lo dibujo
        bottom_left_0R=Point(start_points[1]-ancho/2,rows-1);
        top_right_0R=Point(start_points[1]+ancho/2,rows-1-alto);
        draw_box(bottom_left_0R,top_right_0R, boxes); 
	}

	//Avanzo desde abajo hacia arriba buscando los puntos de la RUTA IZQUIERDA (proceso de apilamiento de cajas)
 	vector<Point> ruta_left;	//aca se almacenarÃ¡n los puntos que forman la ruta en variables x-y
 	int x0=start_points[0];  //semilla inicial donde parto 
 	 
 	ruta(ruta_left,x0,ancho,alto,imagen_warped,boxes); //es el corazÃ³n del cÃ³digo
	
	//print_vector_points(ruta_left);
	//printf("TamaÃ±o de la lista que guarda los puntos de la ruta izquierda: %d\n",ruta_left.size());
	
	
	
	//Avanzo desde abajo hacia arriba buscando los puntos de la RUTA DERECHA
 	vector<Point> ruta_right;	//aca se almacenarÃ¡n los puntos que forman la ruta
 	x0=start_points[1];  //semilla inicial donde parto 
 	ruta(ruta_right,x0,ancho,alto,imagen_warped,boxes); //(las dos rutaas tardan 37ms)
 	
	//print_vector_points(ruta_right);
	//printf("TamaÃ±o de la lista que guarda los puntos de la ruta derecha: %d\n",ruta_right.size());

	//CHEQUEO FINAL
	//printf("primer linea. PIXEL: %d \n segunda linea. PIXEL: %d \n",start_points[0],start_points[1]);    


	//FITEADO DE LOS PUNTOS X-Y DE LA CURVA (recordar que solo miro los puntos cercanos a la base de la imagen)
	//PASO A UNAS VARIABLES X(+ derecha)-Y(+ arriba) RESPECTO A DE LA CAMARA   
	unsigned int order=orden_polinomio; //orden del polinomio que hace el ajuste
	double coefficients_left[order+1]; //coeficientes del polinomio de orden "order"
	double coefficients_right[order+1];
   
	int base[2]; //almacena el punto mas cercano del auto a la ruta en variables x1,y1
  
	
	Mat imagen_final=boxes.clone(); 
	Mat imagen_final_color;//aca se dibujaran los cuadrados y el ajuste de las lineas de ruta
	cvtColor(imagen_final, imagen_final_color, 8, 3);
	draw_box_color(bottom_left_0L,top_right_0L, imagen_final_color);
	draw_box_color(bottom_left_0R,top_right_0R, imagen_final_color); //tardan 5ms

	//esto permite "ver" al auto y punto mas cercano, y no tener que confiar que se estÃ¡n haciendo bien las cosasa por no poder ver la imagen
	//variable global offset_img, puede ponerse en cero si se desea no desplazar la iamgen warpeada.
	desplaza_imagen(imagen_final_color,offset_img); 

	std::string s7 = "Linea IZQ";
	std::string s8 = "Linea DER";
	
	double coefs_concavity[3];
	
	if (start_points[0]>-1){
	    FITEO(ruta_left,order,coefficients_left); //fiteo la linea izq de ruta  (tarda 1ms)
	    
	    FITEO(ruta_left,2,coefs_concavity);
	    concavity[numero]=abs(coefs_concavity[2]);
	    
	    cv::putText(imagen_final_color,s7,cv::Point(0,rows-20),cv::FONT_HERSHEY_DUPLEX,0.5,cv::Scalar(0,0,230),2,false);
	    dibuja_fitteo(coefficients_left,imagen_final_color,order); //ayuda visual
	    
	    min_dist(coefficients_left,order,base);
	    plot_min_dist(base,imagen_final_color);
	    
	    theta_e=angulo(coefficients_left,order,base);    //angulo entre la tangente a la ruta izquierda en el punto mÃ¡s cercano y la orientaciÃ³n del auto
	    
	    dist=track_error(base,1); //distancia desde el auto al punto mas cercano de la ruta izq en mts	    
	}
	else if (start_points[1]>-1){
	    FITEO(ruta_right,order,coefficients_right);
	    
	    FITEO(ruta_right,2,coefs_concavity);
	    concavity[numero]=abs(coefs_concavity[2]);
	    
	    cv::putText(imagen_final_color,s8,cv::Point(cols-40,rows-20),cv::FONT_HERSHEY_DUPLEX,0.5,cv::Scalar(0,0,230),2,false);
	    //cout<<"RIGHT\n"<<endl;
	    dibuja_fitteo(coefficients_right,imagen_final_color,order);
        
	    min_dist(coefficients_right,order,base);
	    plot_min_dist(base,imagen_final_color);
	    
	    theta_e=angulo(coefficients_right,order,base);   
	    //angulo entre la tangente a la ruta derecha en el punto mas cercano y la orientaciÃ³n del auto
	    
	    dist=track_error(base,-1); //distancia desde el auto al punto mas cercano de la ruta der en mts
	    //printf("R: angulo de error: %f [Â°]\nDistancia de error a la lÃ­nea izquierda: %f [m]\n",theta_e,dist); 
	    
	}
	else{
	    theta_e=0;
	    dist=0;
	    printf("R: SE PERDIERON LAS RUTAS!\n");

	    concavity[numero]=-1.0; //valor negativo va a indicar que se perdieron las rutas, la velocidad se frena y el histogrmaa se recrea
	    
	    std::string s7 = "ALERTA!";
	    cv::putText(imagen_final_color,s7,cv::Point(cols/2-(cols/10),rows/2+rows/8),cv::FONT_HERSHEY_DUPLEX,0.5,cv::Scalar(0,0,230),2,false);
	}	
	
	    
    
    //Rellenado en cada paso del bucle temporizado de los punteros que contienen la info. que despuÃ©s saco con la funcion impresor. El tiempo lo guardo en la funcion sim_sensing
    theta_e_list[numero]=theta_e;
    dist_list[numero]=dist;
    


    //std::string s1 = "Concavidad: " + std::to_string(concavity[numero]);
    //cv::putText(imagen_final_color,s1,cv::Point(0,15),cv::FONT_HERSHEY_DUPLEX,0.45,cv::Scalar(0,0,230),2,false);   
    
    /*std::string s2 = "Ancho [pixeles]: " + std::to_string(ancho_ruta);  
    std::string s3 = "Ang. desv. [deg]: " + std::to_string(theta_e);
    std::string s4 = "Error seguim. [cm]: " + std::to_string(100*dist);
    //std::string s5 = "Ang. manejo [deg]: " + std::to_string(delta);
    std::string s6 = "Res.[pix/mm]: " + std::to_string(resolution/1000);
    
    cv::putText(imagen_final_color,s1,cv::Point(0,15),cv::FONT_HERSHEY_DUPLEX,0.45,cv::Scalar(0,0,230),2,false);
    cv::putText(imagen_final_color,s2,cv::Point(0,30),cv::FONT_HERSHEY_DUPLEX,0.45,cv::Scalar(0,0,230),2,false);
    cv::putText(imagen_final_color,s3,cv::Point(0,45),cv::FONT_HERSHEY_DUPLEX,0.45,cv::Scalar(0,0,230),2,false);
    cv::putText(imagen_final_color,s4,cv::Point(0,60),cv::FONT_HERSHEY_DUPLEX,0.45,cv::Scalar(0,0,230),2,false);
    //cv::putText(imagen_final_color,s5,cv::Point(0,75),cv::FONT_HERSHEY_DUPLEX,0.5,cv::Scalar(0,0,230),2,false);
    cv::putText(imagen_final_color,s6,cv::Point(0,75),cv::FONT_HERSHEY_DUPLEX,0.45,cv::Scalar(0,0,230),2,false); //(tarda 10ms esta ayuda visual)
*/
 
    //IMAGENES DE AYUDA
    if (video_camara==0){
	imshow("IMAGEN CRUDA",imagen);
	imshow("IMAGEN FILTRADA",imagen_filtered);
	
	imshow("IMAGEN WARPEADA",imagen_warped);
	
	//imshow("CAJAS para FITEO",boxes);
	
	imshow("CONTROL DE GIRO",imagen_final_color);
	
	//imshow("IMAGEN WARPEADA", imagen_warped_no_filter);
	//imshow( "Imagen obtenida", ruta );
    
	waitKey(0);
    }

    //GUARDO EN MEMORIA RAM LAS IMAGENES CADA 1 SEGUNDO
     
    if ( numero%(muestras) ==0) {
	Mat i1, i2, i3;
	imagen.copyTo(i1);
	imagen_warped.copyTo(i2);
	imagen_final_color.copyTo(i3);
	
	imagenes_RAM[(numero/muestras)*3]=i1;
	imagenes_RAM[(numero/muestras)*3+1]=i2;
	imagenes_RAM[(numero/muestras)*3+2]=i3; 
		
	//cout << "imagen nÂ° "<< numero/muestras<< "\n";
    }
    numero += 1;

    return 1;
    
    /*t0=clock;
    t1 = clock();
    double time = (double(t1-t0)/CLOCKS_PER_SEC)*1000;
    cout << "T_operation [ms]: " << time << endl;*/

}


double STANLEY(double dist,double theta_e,double vel){
    
    if (vel==0){
	return 0.0;
    }
    
       
    double delta=-theta_e+atan2(K_stanley*dist,vel)*180/PI;
    
    //el valor de delta_max es de 30Â° aproximadamente
    if(delta>max_delta){
	    delta=max_delta;
    }
    else if(delta<-max_delta){
	    delta=-max_delta;
    }
    
    return delta;

}

double set_reference_velocity(int i, double* concavity){
	double umbral1=0.00015; //valor correspondiente a rectas
	double umbral2=0.005;	//curvas suaves (10m de la realidad)
	
	double min_vel=1.0;
	double media_vel=2.0;
	double max_vel=2.3;    //[km/h]
	
	int suma=0;
	int anticipacion=40;
	
	if (control_velocidad==0.0){ //control de velocidad apagado
	    return velocity;
	}
	
	else if(i<100){	//todavia no pasaron los 5 segundos iniciales
	    return 0.0;
	}
	
	else{
	    // revisa que no se hayan perdido las rutas  
	    for (int k=anticipacion;k>0;k--){	
			if (concavity[i-k]==-1.0){//perdidad de rutas
			    return 0.0;
			}
			if (concavity[i-k]>umbral2){//rutas estrechas
			    return min_vel;
			}
			if (concavity[i-k]<umbral1){
			    suma+=1;
			}
	    }
	    
	    if (suma==anticipacion){
		return max_vel;
	    }
	    else{
		return media_vel;
	    }
	    
	}

}	

double controlador_P(double error){
	//recibe el error de velocidad, y devuelve PWM_n [+-1] (PWM=PWM_n+100/255)
	double out;
	double GAIN=0.5;//0.253; //TRABAJA CON PWM ENTRE 0-1
	out=error*GAIN;

	double max_value=(255.0-death_zone)/255.0;
	
	//cout<<out<<endl;
	if (out>0 && out<max_value){
	    return out;
	}
	else if(out<0){
	    return -50/255.0; //zona muerta del motor 100-50
	}
	else{
	    return max_value;
	}
	
}

double controlador_PI(double* ERROR,double* ERROR_INT,int* ACCION_CONTROL,int index){
	//recibe el error de velocidad, y devuelve PWM_n [+-1] (PWM=PWM_n+100/255)
	double out;
	double Kp=0.5;
	double Ki=1.5;
	int i=index;
	
	
	if (ACCION_CONTROL[index-1]>=250 || ACCION_CONTROL[index-1]<=5){
	    ERROR_INT[index]=ERROR_INT[index-1];
	}
	else{
	    ERROR_INT[index]=ERROR_INT[index-1]+ERROR[index]*dT;
	}
	
	out=Kp* ( ERROR[index] + Ki * ERROR_INT[index]);
	
	double max_value=(255.0-death_zone)/255.0;
	
	//cout<<out<<endl;
	if (out>0 && out<max_value){
	    return out;
	}
	else if(out<=0){
	    return -death_zone/255; //zona muerta del motor 100-50
	}
	else{
	    //cout<<out<<endl;
	    return max_value;
	}
	
}

double FPB(double u, double* list, int ind){
    double y,fc=2.0;
    double TAU=1/fc;
    if (ind==0){
        return u;
    }
    else{
        //printf("TAU: %f - Indice: %d \n",TAU,ind);
        y= (list[ind-1]*TAU+u*dT) / (TAU+dT);
        
        //printf("sin filtrar: %f - filtrado: %f \n",u,delta_filtrado);
        return y;
    }  
    
}
