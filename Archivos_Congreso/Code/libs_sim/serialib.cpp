/*!
 \file    serialib.cpp
 \brief   Source file of the class serialib. This class is used for communication over a serial device.
 \author  Philippe Lucidarme (University of Angers)
 \version 2.0
 \date    december the 27th of 2019
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
This is a licence-free software, it can be used by anyone who try to build a better world.
 */

#include "serialib.h"

extern "C" {
    #include "extApi.c"
   	#include "extApiPlatform.c"
}

int portNb=19997;
int leftMotorHandle;
int rightMotorHandle;
int sensorHandle;
int clientID;


/* Defino todas los "handles" por afuera para que puedan ser accedidos por
   las funciones de actuacion y sensado */
simxInt motorL, motorR;
simxInt dirL, dirR;
simxInt camera;
simxUChar* raw_image;
int rx = 160;
int ry = 213;
int first_sense = 1;

void sim_init(int clientID) {	
	raw_image = (simxUChar *) malloc(rx*ry*3*sizeof(simxUChar));
	
	simxGetObjectHandle(clientID, "MotorRL", &motorL, simx_opmode_blocking);
	simxGetObjectHandle(clientID, "MotorRR", &motorR, simx_opmode_blocking);
	simxGetObjectHandle(clientID, "DirL", &dirL, simx_opmode_blocking);
	simxGetObjectHandle(clientID, "DirR", &dirR, simx_opmode_blocking);
	simxGetObjectHandle(clientID, "Vision_sensor", &camera, simx_opmode_blocking);
}

void initCamara(){
	
}

Mat getImage(){
	simxInt resolution[2] = {0,0};
	simxUChar options = 0; //0: grayscale, 1: rgb
	
	int param = simx_opmode_buffer;
	if (first_sense == 1) {
		first_sense = 0;
		param = simx_opmode_streaming;
	}
	simxGetVisionSensorImage(clientID, camera, &resolution[0], &raw_image, options, param);

	
	//printf("%d %d\n",resolution[0], resolution[1]);
	//printf("%u %u\n",raw_image[0],raw_image[1]);
	Mat imagen(rx,ry,CV_8UC3,raw_image);
	flip(imagen, imagen, 1);

	return imagen;
}
	


//_____________________________________
// ::: Constructors and destructors :::


/*!
    \brief      Constructor of the class serialib.
*/
serialib::serialib()
{

}


/*!
    \brief      Destructor of the class serialib. It close the connection
*/
// Class desctructor
serialib::~serialib()
{
    closeDevice();
}

/* funcion que frena la simulación actual (si es que hay una)
   y arranca una nueva */
void restart(int clientID){
	int res = simxStopSimulation(clientID,simx_opmode_blocking);
	int is_running = 1;
	while (is_running == 1) {
		int error_code;
		simxInt ping_time = 0;
		simxInt server_state = 0;
		error_code = simxGetPingTime(clientID,&ping_time);
		error_code = simxGetInMessageInfo(clientID,simx_headeroffset_server_state,&server_state);
		is_running = server_state & 1;
	}
	simxStartSimulation(clientID,simx_opmode_blocking);
}

char serialib::openDevice(const char *Device,const unsigned int Bauds)
{
	simxFinish(-1);

    clientID=simxStart((simxChar*)"10.0.2.15",portNb,true,true,5000,5);    //10.73.152.111  // 192.168.43.188   //10.73.152.53
    if (clientID!=-1) {
    	printf("Connected to remote API server\n");
		restart(clientID);
		sim_init(clientID);
		return 1;
		
    } else {
    	printf("Failed connecting to remote API server");
    }
	return -1;

}

void serialib::closeDevice()
{
	simxStopSimulation(clientID,simx_opmode_blocking);
    simxFinish(clientID);
    printf("Exiting remote API server\n");
}



char serialib::writeString(const char *receivedString)
{
	if (receivedString[0] == 'r') {
		return 0;
	}
	int i = 0;
	while (receivedString[i] != '/'){
		i++;
	}
	int valor1 = atoi(string(receivedString).substr(0,i).c_str());
	int j = i;
	while (receivedString[j] != '\n'){
		j++;
	}
 	int valor2 = atoi(string(receivedString).substr(i+1,j).c_str());
	
	simxSetIntegerSignal(clientID,"valor1",(simxInt) valor1,simx_opmode_oneshot);
	simxSetIntegerSignal(clientID,"valor2",(simxInt) valor2,simx_opmode_oneshot);

    return 1;
}

//////////////////////////////////////////////////////////////////////////////////
//FUNCIONES DE LA COMUNICACION SERIE RASPY-ARDUINO
string readline(serialib *serial){
    simxInt data = 0;
	simxGetIntegerSignal(clientID,"variable1",&data,simx_opmode_oneshot);
    return to_string(data);
}

void clear_buffer(serialib *serial){
	for (int i=0; i<3; i++){
		getImage();
		sleep(1);
	}
}
///////////////////////////////////////////////////////////////////////////////////

