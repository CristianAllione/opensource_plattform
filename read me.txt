This supplementary material contains:

*3D model of the real 3d printed 1:10 scale car in "3D CAD Models" folder.
Every pieces in .par format and assembly in .asm format can be loaded in 
Solidedge software.

*"Code" folder contains libraries "libs_real" and "libs_sim" neccesary for runing in both real or 
simulated plattforms. "codigo.cpp" contains C++ code that makes use of functiones in "funciones.cpp". 
By means of the "Makefile" is posible to run the whole code inserting "make real" or "make sim" in terminal.

*The "Videos" contains the videos of the simulated and real runs, as well as the side-by-side comparison.